package com.example.roomexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.roomexample.database.AppDatabase;
import com.example.roomexample.database.MahasiswaModel;
import com.example.roomexample.view.ReadActivity;

public class MainActivity extends AppCompatActivity {

    private AppDatabase appDatabase;
    private Button btnLihatData, btnSimpan;
    private EditText etNim,etNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLihatData = findViewById(R.id.activitymain_btn_lihatdata);
        btnSimpan = findViewById(R.id.activitymain_btn_simpan);

        etNama = findViewById(R.id.activitymain_et_nama);
        etNim = findViewById(R.id.activitymain_et_nim);

        appDatabase = AppDatabase.initDatabase(getApplicationContext());

        btnSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    MahasiswaModel mahasiswaModel = new MahasiswaModel();

                    mahasiswaModel.setNama(etNama.getText().toString());
                    mahasiswaModel.setNim(etNim.getText().toString());

                    appDatabase.mahasiswaDAO().insertMahasiswa(mahasiswaModel);

                    Log.d("MainAcitity" , "sukses ");
                    Toast.makeText(getApplicationContext(),"Tersimpan", Toast.LENGTH_SHORT).show();
                }catch (Exception ex){
                    Log.e("MainAcitity" , "gagal menyimpan , msg : "+ex.getMessage());
                    Toast.makeText(getApplicationContext(),"Gagal Menyimpan", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnLihatData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ReadActivity.class);

                startActivity(intent);

            }
        });
    }
}
